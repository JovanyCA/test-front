import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDummyComponent } from './form-dummy/form-dummy.component';
import { TrademarksDetailComponent } from './trademarks-detail/trademarks-detail.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'buscador',
    pathMatch: 'full'
  },
  {
    path:'buscador',
    component: FormDummyComponent    
  },
  {
    path:'trademarks-detail',
    component: TrademarksDetailComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
