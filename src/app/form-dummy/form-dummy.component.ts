import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, retry } from "rxjs/operators";
import { Router } from '@angular/router';
import { MessageService } from '../services/message.service';

export class Trademark {
  marca: string;
  selected: boolean;

  constructor(marca: string, selected?: boolean) {
    if (selected === undefined) {
      this.selected = false;
    } else {
      this.selected = selected;
    }
    this.marca = marca;
  }
}

export class SocialGroup {
  social: string;
  trademarks: Trademark[] = [];

  constructor(social?: string, trademark?: Trademark) {
    this.social = social;
    this.trademarks.push(trademark);
  }
}

export const _filter = (opt1: Trademark[], value: string): Trademark[] => {

  let opt: string[];
  opt = opt1.map(x=>x.marca);
  const filterValue = value.toLowerCase();
  let rt = opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);

  let tmArr = [];
  rt.map(a => {
    tmArr.push(new Trademark(a));
  });
  return tmArr;
};

@Component({
  selector: 'app-form-dummy',
  templateUrl: './form-dummy.component.html',
  styleUrls: ['./form-dummy.component.scss']
})

export class FormDummyComponent implements OnInit {
  form:FormGroup;

  //Dummy Data
  paises = [
    "Argentina",
    "Bolivia",
    "Brasil",
    "Chile",
    "Colombia",
    "Costa Rica",
    "Cuba",
    "Ecuador",
    "El Salvador",
    "Guayana Francesa",
    "Granada",
    "Guatemala",
    "Guayana",
    "Haití",
    "Honduras",
    "Jamaica",
    "México",
    "Nicaragua",
    "Paraguay",
    "Panamá",
    "Perú",
    "Puerto Rico",
    "República Dominicana",
    "Surinam",
    "Uruguay",
    "Venezuela"
  ];
  socialGroup: SocialGroup[] = [
    {
      social: "Facebook",
      trademarks: [
        { marca: "Adidas", selected: false },
        { marca: "Bimbo", selected: false },
        { marca: "Lala", selected: false }
      ]
    },
    {
      social: "Twitter",
      trademarks: [
        { marca: "Adidas", selected: false },
        { marca: "Cemex", selected: false },
        { marca: "Telcel", selected: false }
      ]
    },
    {
      social: "Instagram",
      trademarks: [
        { marca: "Helvex", selected: false },
        { marca: "Lala", selected: false },
        { marca: "Nike", selected: false }
      ]
    }
  ];

  socialGroupOptions: Observable<SocialGroup[]>;
  selectedTrademarks: SocialGroup[] = new Array<SocialGroup>();
  
  constructor(private router: Router,
              private messageService: MessageService) { 

    this.form = new FormGroup({
      paises: new FormControl(),
      marca: new FormControl(),
      searchTrademark: new FormControl()
    });

  }

  ngOnInit() {
    this.socialGroupOptions = this.form
      .get("searchTrademark")!
      .valueChanges.pipe(
        startWith(""),
        map(value => this._filterGroup(value))
      );
  }

private _filterGroup(value: string): SocialGroup[] {
  console.log('_filterGroup->',value);

  if (value) {
    if(typeof(value) != typeof("")) value = value[0]? value[0]: '';
    return this.socialGroup
      .map(group => ({social: group.social, trademarks: _filter(group.trademarks, value)}))
      .filter(group => group.trademarks.length > 0);
  }

  return this.socialGroup;

}

optionClicked(event: Event, group: SocialGroup, i: number) {
  event.stopPropagation();
  this.toggleSelection(group, i);
}

toggleSelection(group: SocialGroup, i: number) {
  group.trademarks[i].selected = !group.trademarks[i].selected;
  /* console.log("Index:", i);
  console.log(group.trademarks[i].marca);
  console.log(group.trademarks[i].selected); */

  // Add Trademark
  if (group.trademarks[i].selected) {
    // Social Exist
    let rs = this.selectedTrademarks.findIndex(
      value => value.social === group.social
    );
    if (rs != -1) {
      console.log(`Existe Red Social: ${group.social} en posición:`, rs);
      // Treademark Exist
      let mk = this.selectedTrademarks[rs].trademarks.findIndex(
        value => value.marca === group.trademarks[i].marca
      );
      if (mk != -1) {
        console.log(
          `La marca ${group.trademarks[i].marca} ya existe: `,
          group.trademarks[i].marca
        );
        this.selectedTrademarks[rs].trademarks[mk].selected = true;
      }
      // Treademark No Exist
      else {
        console.log(
          `La marca ${group.trademarks[i].marca} NO existe: `,
          group.trademarks[i].marca
        );
        this.selectedTrademarks[rs].trademarks.push(
          new Trademark(group.trademarks[i].marca, true)
        );
      }
    }
    // Social No Exist
    else {
      console.log(`No Existe Red Social: ${group.social}, agregando..`);
      let tmpTM = new Trademark(group.trademarks[i].marca, true);
      let tmpRS = new SocialGroup(group.social, tmpTM);

      this.selectedTrademarks.push(tmpRS);
    }
  }
  // Delete Trademark
  else {
    // Social Exist
    let rs = this.selectedTrademarks.findIndex(
      value => value.social === group.social
    );
    if (rs != -1) {
      console.log(`Existe Red Social: ${group.social} en posición:`, rs);
      // Treademark Exist
      let mk = this.selectedTrademarks[rs].trademarks.findIndex(
        value => value.marca === group.trademarks[i].marca
      );
      if (mk != -1) {
        console.log(
          `La marca ${group.trademarks[i].marca} ya existe: `,
          group.trademarks[i].marca
        );
        //this.selectedTrademarks[rs].trademarks[mk]
        this.selectedTrademarks[rs].trademarks.splice(mk, 1);
        console.log(`La marca ${group.trademarks[i].marca} eliminado `);
      }
      // Treademark No Exist
      else {
        console.log(`La marca ${group.trademarks[i].marca} NO existe `);
      }
    }
    // Social No Exist
    else {
      console.log(`Existe Red Social: ${group.social} NO existe`);
    }
  }

  let mr2 = [];
  this.selectedTrademarks.forEach(element1 => {
    element1.trademarks.forEach(element2 => {
      mr2.push(element2.marca);
    });
  });

}

btnClick= function () {

  if(this.selectedTrademarks.length <= 0){
      alert('No ha seleccionado marcas');
  }else{
    console.log(this.selectedTrademarks);
    this.messageService.test = this.selectedTrademarks;   
    this.router.navigateByUrl('/trademarks-detail');
  }
  
};

goToVideo() {
  window.open("https://www.youtube.com/watch?v=ZRCdORJiUgU&feature=youtu.be", "_blank");
}


}
