import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-trademarks-detail',
  templateUrl: './trademarks-detail.component.html',
  styleUrls: ['./trademarks-detail.component.css']
})
export class TrademarksDetailComponent implements OnInit {
  message:any;
  
  constructor(private messageService: MessageService) {
    this.message = this.messageService.test;
   }

  ngOnInit(): void {
  }

}
