import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrademarksDetailComponent } from './trademarks-detail.component';

describe('TrademarksDetailComponent', () => {
  let component: TrademarksDetailComponent;
  let fixture: ComponentFixture<TrademarksDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrademarksDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrademarksDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
